import vcf


def get_variant_annotation(node, vcfname, annotation):
    chrom = node.key.chromosome
    start = node.key.position
    end = start + node.db.schema.granularity - 1
    v = vcf.Reader(filename=vcfname)
    recs = v.fetch(chrom, start, end)
    for rec in recs:
        ann = rec.INFO[annotation]
        pos = rec.POS
        node.assign(pos, ann)
