import os
import sys

import genomics
from genomics import db
from genomics import mapreduce
from genomics import organism


annot = sys.argv[1]

types = {'DP': int, 'MQ': float}

genome = organism.genome_db['Ag']
schema = db.GenomeSchema(1000000, types[annot], genome)
db_dir = '/home/tra/db/%s' % annot
mdb = db.DB(db_dir, schema, True)
for key, val in mdb.get_values():
    print(key, val)
