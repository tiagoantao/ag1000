import os
import sys

import genomics
from genomics import db
from genomics import mapreduce
from genomics import organism


import adhoc
#adhoc.get_variant_annotation(node, vcfname, 'MQ')

annot = sys.argv[1]
vcfname = '/data/atroparvus/tra/ag/phase2/all.vcf.gz'

types = {'DP' : int, 'MQ': float}


def dbmap(db, fun, *args):
    '''
    fuction is fqdn
    module dir has to be on the python path cross grid
    '''
    for key in db.schema.enumerate_node_keys():
        print(key)
        node = db.get_write_node(key)
        paras = [fun.__module__ + '.' + fun.__name__]
        paras.append(node)
        paras.extend(args)
        fname = mapreduce.pickle_in(paras)
        genomics.lexec.submit('python3', '-m genomics.mapreduce.runner %s' % fname)
    genomics.lexec.wait(for_all=True)

genome = organism.genome_db['Ag']
schema = db.GenomeSchema(1000000, types[annot], genome)
db_dir = '/home/tra/db/%s' % annot
try:
    os.makedirs(db_dir)
except FileExistsError:
    pass  # OK
db = db.DB(db_dir, schema, True)

dbmap(db, adhoc.get_variant_annotation, vcfname, annot)
