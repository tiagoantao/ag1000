import pickle
import statistics

chroms = ['X', '3L', '3R', '2L', '2R', 'Y_unplaced', 'UNKN']

smp_dps = {}
for chrom in chroms:
    try:
        smp_dps[chrom] = pickle.load(open(chrom + '.pickle', 'rb'))
    except FileNotFoundError:
        pass


def get_median(cnts):
    dps = []
    for dp, cnt in cnts.items():
        dps.extend([dp] * cnt)
    return statistics.median(dps)

inds = smp_dps['X'].keys()
for ind in inds:
    vals = [ind]
    for chrom in chroms:
        try:
            chrom_dps = smp_dps[chrom]
            vals.append(str(get_median(chrom_dps[ind])))
        except (KeyError, statistics.StatisticsError):
            vals.append('0')
    print("\t".join(vals))
