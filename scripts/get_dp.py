from collections import defaultdict
import pickle
import sys

import vcf

vcfname = sys.argv[1]
pname = sys.argv[2]

f = vcf.Reader(filename=vcfname)
rec = next(f)

smp_dp = {}

for sample in rec.samples:
    smp_dp[sample.sample] = defaultdict(int)

f = vcf.Reader(filename=vcfname)

for i, rec in enumerate(f):
    if i % 1000 == 0:
        print(rec.POS, i)
    if not rec.is_snp:
        continue
    for sample in rec.samples:
        indiv = sample.sample
        dp = sample['DP']
        if dp is None:
            continue
        smp_dp[indiv][dp] += 1

w = open(pname, 'wb')
pickle.dump(smp_dp, w)
w.close()
