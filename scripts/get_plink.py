import sys

import vcf

vcfname = sys.argv[1]
my_chrom = sys.argv[2]


sexf = open('dps')
sexes = {}
smp_dp = {}
for l in sexf:
    l = l.rstrip().split('\t')
    ind = l[0]
    smp_dp[ind] = dp = {}
    # Ugly...
    dpx = float(l[1])
    dp3l = float(l[2])
    dp3r = float(l[3])
    dp2l = float(l[4])
    dp2r = float(l[5])
    dpy = float(l[6])
    dpunkn = float(l[7])
    if dpx / dp3l < 0.75:
        sexes[ind] = '1'
    else:
        sexes[ind] = '2'
    dp['X'] = dpx
    dp['3L'] = dp3l
    dp['3R'] = dp3r
    dp['2L'] = dp2l
    dp['2R'] = dp2r
    dp['Y_unplaced'] = dpy
    dp['UNKN'] = dpunkn

chroms = {"2L": 2, "2R": 12, "3L": 3, "3R": 13, "Y_unplaced": 21, "UNKN": 22, "X": 23}


def accept_position(rec):
    if not rec.is_snp:
        return False
    if len(rec.ALT) != 1:
        return False
    if rec.INFO["MQ"] < 40:
        return False
    if rec.INFO["QD"] < 5:
        return False
    if rec.INFO["HRun"] > 3:
        return False
    return True


def accept_sample(smp, sex, dp):
    if smp["GQ"] < 40:
        return False
    if sex == "1" and smp.site.CHROM == "X":
        if smp["DP"] < 5 or smp["DP"] > dp * 2:
            return False
    elif smp["DP"] < dp / 2 or smp["DP"] > dp * 2:
        return False
    return True


def call(sample):
    if sample['PL'] is None:
        return None
    return sample.gt_alleles

all_indivs = list(smp_dp.keys())
all_indivs.sort()

ind_file = {}
for ind in all_indivs:
    ind_file[ind] = open(ind + '.tmp' + my_chrom, 'wt', encoding='utf8')
    ind_file[ind].write('%s %s 0 0 %s -9' % (ind, ind, sexes[ind]))

f = vcf.Reader(filename=vcfname)
wmap = open(my_chrom + '.map', 'wt', encoding='utf8')
for i, rec in enumerate(f):
    if not accept_position(rec):
        continue
    tot_alleles = set()
    indiv_alleles = {}
    for sample in rec.samples:
        indiv = sample.sample
        alleles = call(sample)
        if alleles is None:
            continue
        if accept_sample(sample, sexes[indiv], smp_dp[indiv][rec.CHROM]):
            tot_alleles = tot_alleles.union(alleles)
            indiv_alleles[indiv] = alleles
    if len(tot_alleles) != 2:
        continue
    alls = [rec.REF, rec.ALT[0]]
    wmap.write("%s\t%s%d-%s\t0\t%d\n" % (chroms[rec.CHROM], rec.CHROM,
                                         rec.POS, rec.REF, rec.POS))
    for ind, w in ind_file.items():
        if ind in indiv_alleles:
            alleles = indiv_alleles[ind]
            w.write(' %s %s' % (alls[int(alleles[0])],
                                alls[int(alleles[1])]))
        else:
            w.write(' 0 0')
    if i % 10000 == 0:
        print(i, rec.CHROM, rec.POS)

wmap.close()

for iw in ind_file.values():
    iw.close()

w = open(my_chrom + '.ped', "w")
for ind in all_indivs:
    f = open(ind + '.tmp' + my_chrom, 'rt', encoding='utf8')
    w.write(f.readline() + "\n")
    #os.remove(ind + ".tmp")
w.close()
